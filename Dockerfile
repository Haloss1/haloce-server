FROM i386/alpine:3.13.10

RUN apk add --no-cache wine freetype ncurses

WORKDIR /server

COPY game-files/ .

ENV PORT 2302

CMD wineconsole --backend=curses haloceded