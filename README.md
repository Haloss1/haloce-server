# Use prebuilt image:

### Run:
```
docker run -itd -p 2302:2302/udp registry.gitlab.com/haloss1/haloce-server:latest
```

# Build your own:

### Build (Replace `ImageName` and `Tag`):
```
docker build -t ImageName:Tag .
```

### Run (Use `ImageName` and `Tag` from your build):
```
docker run -itd -p 2302:2302/udp ImageName:Tag
```